FROM ubuntu:16.04
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

WORKDIR /opt/ibm

ENV APPD_CONTROLLER_HOST="" \
    APPD_CONTROLLER_PORT="8090" \
    APPD_CONTROLLER_SSL=0 \
    APPD_APP="ACE" \
    APPD_TIER="ace-integration" \
    APPD_ACCOUNT="customer1" \
    APPD_KEY="" \
#    QUEUE_MANAGER="QM1" \
    ACE_USER="aceuser" \
    APPD_EXIT="AppDynamicsUserExit" \
    APPD_DIR="/opt/appdynamics/" \
    ACE_INT_HOME="/opt/ibm/ace/integration/" \
    APPD_LOG_LEVEL="info"

#NOT INCLUDED INTO THE REPOSITORY
COPY installer/11.0.0-ACE-LINUXX64-FP0003.tar.gz /tmp/ace.tar.gz
#COPY installer/mqadv_dev910_ubuntu_x86-64.tar.gz /tmp/mq.tar.gz
COPY installer/iib-x64-linux-4.5.3.0.tbz2 /tmp/iib-agent.tbz2

#Installation Scripts
COPY script/kernel_settings.sh /tmp/
COPY script/start.sh /

#Config Files
COPY config/server.conf.yaml /opt/ibm/ace/integration/
#COPY config/ibm_mq_jms_setup.scp /home/${ACE_USER}/
COPY config/controller-info.xml /opt/appdynamics/

RUN apt-get update && \
    apt-get -y install --no-install-recommends sudo bsdtar
RUN chmod +x /tmp/kernel_settings.sh && \
    /tmp/kernel_settings.sh && \
    bsdtar -xvz -C ./ace/ --strip-components=1 -f /tmp/ace.tar.gz && \
    ./ace/ace make registry global accept license silently && \
    useradd --create-home --home-dir /home/${ACE_USER} --shell /bin/bash -G mqbrkrs,sudo ${ACE_USER} && \
    sed -e 's/^%sudo.*/%sudo    ALL=NOPASSWD:ALL/g' -i /etc/sudoers && \
    echo "source /opt/ibm/ace/server/bin/mqsiprofile > /dev/null " >> /home/${ACE_USER}/.bashrc && \
#    mkdir ./mq && \
#    bsdtar -xvz -C ./mq/ --strip-components=1 -f /tmp/mq.tar.gz && \
#   /opt/ibm/mq/mqlicense.sh -accept -text_only && \
#    dpkg -i ./mq/ibmmq-runtime_*.deb && \
#    dpkg -i ./mq/ibmmq-server_*.deb && \
#    dpkg -i ./mq/ibmmq-java_*.deb && \
#    dpkg -i ./mq/ibmmq-jre_*.deb && \
#    dpkg -i ./mq/ibmmq-explorer_*.deb && \
#    dpkg -i ./mq/ibmmq-samples_*.deb && \
#    /opt/mqm/bin/setmqinst -i -p /opt/mqm && \
#    useradd --create-home --home-dir /home/${ACE_USER} --shell /bin/bash -G mqbrkrs,mqm,sudo ${ACE_USER} && \
#    usermod -G mqm root && \
#    usermod -G mqm ${ACE_USER} && \
#    sed -i "s#PROVIDER_URL=file.*#PROVIDER_URL=file:///home/${ACE_USER}/binding#g" /opt/mqm/java/bin/JMSAdmin.config && \
#    mkdir -p /home/${ACE_USER}/binding && \
#    chown ${ACE_USER}:${ACE_USER} /home/${ACE_USER}/binding && \
    bsdtar -xvz -C /opt/ -f /tmp/iib-agent.tbz2 && \
    sed -e "s@\${APPD_EXIT}@${APPD_EXIT}@g" \
        -e "s@\${APPD_DIR}@${APPD_DIR}@g" \
        -i ${APPD_DIR}/controller-info.xml && \
    sed -e "s@\${APPD_EXIT}@${APPD_EXIT}@g" \
        -e "s@\${APPD_DIR}@${APPD_DIR}@g" \
        -i ${ACE_INT_HOME}/server.conf.yaml && \
    chown -R aceuser:aceuser /opt/appdynamics && \
    chown -R aceuser:aceuser /opt/ibm/ace/integration/

#EXPOSE 4414 7800 5672 9180 7600
EXPOSE 7800 7600

USER ${ACE_USER}

CMD ["/start.sh"]