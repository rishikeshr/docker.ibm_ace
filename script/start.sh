#!/bin/bash

source /opt/ibm/ace/server/bin/mqsiprofile

#sudo crtmqm -q -d TQ1 -u SYSTEM.DEAD.LETTER.QUEUE ${QUEUE_MANAGER}
#strmqm ${QUEUE_MANAGER}

#QUEUES=( \
#        "MQ_OUTPUT" \
#        "MQ_REPLY" \
#        "JMS_OUTPUT" \
#        "JMS_REPLY" \
#    )
#for QUEUE in ${QUEUES[@]}; do
#    echo "DEFINE QLOCAL(${QUEUE})" | runmqsc ${QUEUE_MANAGER}
#    setmqaut -m QM1 -n "${QUEUE}" -t q -p ${ACE_USER} -remove
#    setmqaut -m QM1 -n "${QUEUE}" -t q -p ${ACE_USER} +passall +passid +setall +setid +browse +get +inq +put +set
#done

#setmqaut -m ${QUEUE_MANAGER} -t qmgr -p ${ACE_USER} -all
#setmqaut -m ${QUEUE_MANAGER} -t qmgr -p ${ACE_USER} +setall +setid +altusr +connect +inq

#echo "REFRESH SECURITY TYPE(AUTHSERV)" | runmqsc ${QUEUE_MANAGER}

#/opt/mqm/java/bin/JMSAdmin -t -v -cfg /opt/mqm/java/bin/JMSAdmin.config < \
#    /home/${ACE_USER}/ibm_mq_jms_setup.scp
#/opt/mqm/bin/runmqsc < /opt/mqm/java/bin/MQJMS_PSQ.mqsc
#dspmq

if [ -e ${APPD_DIR}controller-info.xml.backup ]
then
    cp ${APPD_DIR}controller-info.xml.backup ${APPD_DIR}controller-info.xml
else
    cp ${APPD_DIR}controller-info.xml ${APPD_DIR}controller-info.xml.backup
fi

sed -e "s@\${APPD_CONTROLLER_HOST}@${APPD_CONTROLLER_HOST}@g" \
    -e "s@\${APPD_CONTROLLER_PORT}@${APPD_CONTROLLER_PORT}@g" \
    -e "s@\${APPD_CONTROLLER_SSL}@${APPD_CONTROLLER_SSL}@g" \
    -e "s@\${APPD_APP}@${APPD_APP}@g" \
    -e "s@\${APPD_TIER}@${APPD_TIER}@g" \
    -e "s@\${APPD_ACCOUNT}@${APPD_ACCOUNT}@g" \
    -e "s@\${APPD_KEY}@${APPD_KEY}@g" \
    -e "s@\${APPD_LOG_LEVEL}@${APPD_LOG_LEVEL}@g" \
    -i ${APPD_DIR}/controller-info.xml

IntegrationServer --work-dir ${ACE_INT_HOME}